document.addEventListener('DOMContentLoaded', () => {
  // creating of book items
  createBookItems();

  document.getElementById('form').addEventListener('submit', accept);

  document.getElementById('books').addEventListener('click', (event) => {
    if (event.target) {
      if (event.target.classList.contains('bookStore__button_edit')) {
        edit(event.target);
      }
      if (event.target.classList.contains('bookStore__button_delete')) {
        delele(event.target);
      }
    }
  });
});

// generating of a guid for book item
function generateGUID() {
  const GUID = Math.random().toString(36).substring(2) + Date.now().toString(36);
  return GUID;
}

// getting of the local storage
function getLocalStorage() {
  const oldBookItems = JSON.parse(localStorage.getItem('bookItems')) || {};
  return oldBookItems;
}

// creating of book items (books list)
function createBookItems() {
  const containerBookItems = document.getElementById('books');
  const books = getLocalStorage();
  let bookItem = '';

  for (const element in books) {
    const obj = books[element];
    bookItem += `<div data-uuid="${element}" class="bookStore__item mb-4">
      <div class="bookStore__textBlock">
        <p class="bookStore__text bookStore__text_author">${obj.author}</p>
        <p class="bookStore__text bookStore__text_title">${obj.title}</p>
      </div>
      <div class="bookStore__buttonBlock">
        <button class="bookStore__button bookStore__button_delete btn btn-danger">Удалить</button>
        <button class="bookStore__button bookStore__button_edit btn btn-secondary">Редактировать</button>
      </div>
    </div>`;
  }
  containerBookItems.innerHTML = bookItem;
}

// accpetion of a book item modification
function accept(e) {
  e.preventDefault();

  let uuid = generateGUID();
  const books = getLocalStorage();

  if (this.elements.uuid.value) {
    uuid = this.elements.uuid.value;
  }

  // validation
  for (let i = 0; i < this.elements.length; i++) {
    if (this.elements[i].classList.contains('form__input') && this.elements[i].value === '') {
      const alertWindow = alert('Пустые поля нельзя сохранить. Заполните их');
      return;
    }
  }

  books[uuid] = {
    author: this.elements.author.value,
    title: this.elements.title.value,
    year: this.elements.year.value,
    pages: this.elements.pages.value,
  };

  localStorage.setItem('bookItems', JSON.stringify(books));

  this.reset();
  this.elements.uuid.removeAttribute('value');

  createBookItems();
}

// editing of a target book item
function edit(e) {
  const books = getLocalStorage();
  const form = document.getElementById('form');
  const { uuid } = e.parentElement.parentElement
    .dataset;
  const book = books[uuid];

  form.elements.uuid.value = uuid;
  form.elements.author.value = book.author;
  form.elements.title.value = book.title;
  form.elements.year.value = book.year;
  form.elements.pages.value = book.pages;
}

// deleting of a target book item
function delele(e) {
  const modalWindowConfirm = confirm('Удалить элемент?');
  const books = getLocalStorage();
  const { uuid } = e.parentElement.parentElement.dataset;

  if (!modalWindowConfirm) {
    return;
  }

  delete books[uuid];

  localStorage.setItem('bookItems', JSON.stringify(books));

  createBookItems();
}
