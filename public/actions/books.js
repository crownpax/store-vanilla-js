"use strict";

document.addEventListener('DOMContentLoaded', function () {
  // creating of book items
  createBookItems();
  document.getElementById('form').addEventListener('submit', accept);
  document.getElementById('books').addEventListener('click', function (event) {
    if (event.target) {
      if (event.target.classList.contains('bookStore__button_edit')) {
        edit(event.target);
      }

      if (event.target.classList.contains('bookStore__button_delete')) {
        delele(event.target);
      }
    }
  });
}); // generating of a guid for book item

function generateGUID() {
  var GUID = Math.random().toString(36).substring(2) + Date.now().toString(36);
  return GUID;
} // getting of the local storage


function getLocalStorage() {
  var oldBookItems = JSON.parse(localStorage.getItem('bookItems')) || {};
  return oldBookItems;
} // creating of book items (books list)


function createBookItems() {
  var containerBookItems = document.getElementById('books');
  var books = getLocalStorage();
  var bookItem = '';

  for (var element in books) {
    var obj = books[element];
    bookItem += "<div data-uuid=\"".concat(element, "\" class=\"bookStore__item mb-4\">\n      <div class=\"bookStore__textBlock\">\n        <p class=\"bookStore__text bookStore__text_author\">").concat(obj.author, "</p>\n        <p class=\"bookStore__text bookStore__text_title\">").concat(obj.title, "</p>\n      </div>\n      <div class=\"bookStore__buttonBlock\">\n        <button class=\"bookStore__button bookStore__button_delete btn btn-danger\">\u0423\u0434\u0430\u043B\u0438\u0442\u044C</button>\n        <button class=\"bookStore__button bookStore__button_edit btn btn-secondary\">\u0420\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u0442\u044C</button>\n      </div>\n    </div>");
  }

  containerBookItems.innerHTML = bookItem;
} // accpetion of a book item modification


function accept(e) {
  e.preventDefault();
  var uuid = generateGUID();
  var books = getLocalStorage();

  if (this.elements.uuid.value) {
    uuid = this.elements.uuid.value;
  } // validation


  for (var i = 0; i < this.elements.length; i++) {
    if (this.elements[i].classList.contains('form__input') && this.elements[i].value === '') {
      var alertWindow = alert('Пустые поля нельзя сохранить. Заполните их');
      return;
    }
  }

  books[uuid] = {
    author: this.elements.author.value,
    title: this.elements.title.value,
    year: this.elements.year.value,
    pages: this.elements.pages.value
  };
  localStorage.setItem('bookItems', JSON.stringify(books));
  this.reset();
  this.elements.uuid.removeAttribute('value');
  createBookItems();
} // editing of a target book item


function edit(e) {
  var books = getLocalStorage();
  var form = document.getElementById('form');
  var uuid = e.parentElement.parentElement.dataset.uuid;
  var book = books[uuid];
  form.elements.uuid.value = uuid;
  form.elements.author.value = book.author;
  form.elements.title.value = book.title;
  form.elements.year.value = book.year;
  form.elements.pages.value = book.pages;
} // deleting of a target book item


function delele(e) {
  var modalWindowConfirm = confirm('Удалить элемент?');
  var books = getLocalStorage();
  var uuid = e.parentElement.parentElement.dataset.uuid;

  if (!modalWindowConfirm) {
    return;
  }

  delete books[uuid];
  localStorage.setItem('bookItems', JSON.stringify(books));
  createBookItems();
}