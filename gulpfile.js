const path = require('path');

const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');

sass.compiler = require('node-sass');

// ---DEVELOP BUILD---

// converting of sass files
function sassConvertDev() {
  return src('frontend/styles/*.scss')
    .pipe(sass())
    .pipe(dest('frontend/styles/'));
}

// ---PRODACTION BUILD---

// html prod build
function htmlProd() {
  return src('frontend/index.html')
    .pipe(dest('public/'));
}

// css prod build
function sassProd() {
  return src('frontend/styles/main.scss')
    .pipe(sass())
    .pipe(dest('public/styles/'));
}

// js prod build
function jsProd() {
  return src('frontend/actions/*.js')
    .pipe(babel({
      presets: ['@babel/env'],
    }))
    .pipe(dest('public/actions/'));
}

// ---EXPORTS GULP TASK---

exports.watch = function () {
  watch('frontend/styles/*.scss', { events: ['add', 'change'], delay: 200 }, sassConvertDev);
};

exports.build = series(htmlProd, sassProd, jsProd);
